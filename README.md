## Table of contents

- Requirements
- Installation
- Permissions
- Configuration and Usage
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Permissions

The following permission options are available for "Author bulk assignment" and should be assigned to the relevant user roles:

- "Administer author bulk assignment settings" grants users the ability to configure the module and set the allowed entity types.
- "Assign author to selected content" permits users to assign selected content to a specific author.

## Configuration and Usage

- After installing the module, navigate to the module's configuration page by going to Configuration => Content authoring => Author bulk assignment settings.
  - Note that the configuration page is accessible only to users with the "Administer author bulk assignment settings" permission.
- On this page, select the entity types for which you want to enable the author assignment option within bulk operations and hit "Save". 
  - This action creates an individual Action configuration entity for each of the selected entity types.
- Include the "Assign bulk content item to author" action in the bulk operation view field configuration for your admin listing views.
  - Users with the "Assign author to selected content" permission will be able to use the author assignment bulk operation.
- Note that if any of the selected items lack “Edit” permissions for the selected assignee, specific items will not be assigned to the new user. 
  - Drupal will display a warning message indicating which items were not updated.
  - For example, this scenario occurs when the selected entity items have node types or media types that the assignee is not authorised to edit.

## Maintainers

- Fathima N Asmat - [fathimaasmat](https://www.drupal.org/u/fathimaasmat)

