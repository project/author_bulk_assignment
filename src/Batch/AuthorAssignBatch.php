<?php

namespace Drupal\author_bulk_assignment\Batch;

use Drupal\Core\Entity\EntityInterface;
use Drupal\system\Entity\Action;

/**
 * Processes entities in chunks to re-save them with updated author.
 *
 * @package Drupal\author_bulk_assignment\Batch
 */
class AuthorAssignBatch {

  /**
   * Assign author batch operation callback.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity in process.
   * @param \Drupal\system\Entity\Action $action
   *   Author bulk assignment action.
   * @param int $assignee_uid
   *   Assignee uid.
   * @param array|\DrushBatchContext $context
   *   The sandbox context.
   */
  public static function assignAuthor(EntityInterface $entity, Action $action, int $assignee_uid, &$context) {
    $message = t('Updating author for the selected entities ...');
    $action->getPlugin()->setAssignee($assignee_uid);
    $action->execute([$entity]);
    $context['message'] = $message;
    $context['results'][] = $entity;
  }

  /**
   * Finished batch callback for author assignment.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param array $results
   *   The value set in $context['results'] by callback_batch_operation().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public static function assignAuthorFinishedCallback(bool $success, array $results, array $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $action = 'Author assignment';
      $message = \Drupal::translation()->formatPlural(
        count($results),
        '%action was applied to @count item.', '%action was applied to @count items.', [
          '%action' => $action,
        ]);
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}
