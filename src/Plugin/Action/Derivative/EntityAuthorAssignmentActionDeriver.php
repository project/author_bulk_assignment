<?php

namespace Drupal\author_bulk_assignment\Plugin\Action\Derivative;

use Drupal\Core\Action\Plugin\Action\Derivative\EntityActionDeriverBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Provides an action deriver that finds entity types with an owner aka uid.
 *
 * @see \Drupal\author_bulk_assignment\Plugin\Action\AuthorAssignmentBulkAction
 */
class EntityAuthorAssignmentActionDeriver extends EntityActionDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (empty($this->derivatives)) {
      $definitions = [];
      foreach ($this->getApplicableEntityTypes() as $entity_type_id => $entity_type) {
        $definition = $base_plugin_definition;
        $definition['type'] = $entity_type_id;
        $definition['label'] = $this->t('Assign @entity_type to author', [
          '@entity_type' => strtolower($entity_type->getSingularLabel()),
        ]);
        $definitions[$entity_type_id] = $definition;
      }
      $this->derivatives = $definitions;
    }

    return $this->derivatives;
  }

  /**
   * {@inheritdoc}
   */
  protected function isApplicable(EntityTypeInterface $entity_type) {
    return $entity_type->hasKey('uid') || $entity_type->hasKey('owner');
  }

}
