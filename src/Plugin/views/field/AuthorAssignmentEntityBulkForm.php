<?php

namespace Drupal\author_bulk_assignment\Plugin\views\field;

use Drupal\author_bulk_assignment\Plugin\views\field\traits\AuthorAssignmentBulkFormTrait;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a user operations bulk form element.
 *
 * @ViewsField("author_assignment_entity_bulk_form")
 */
class AuthorAssignmentEntityBulkForm extends BulkForm {

  use AuthorAssignmentBulkFormTrait;

}
